<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
	<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
	<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Queue Processing Service</a>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="starter-template">
			<h1>Running Procedure</h1>
			<h4>Step: ${stepLoadData}</h4><a href="http://localhost:8080/createAndLoadData/5000000"><button>Create Data File</button></a>
			<h4>Step: ${stepRunService}</h4><a href="http://localhost:8080/process"><button>Run Service</button></a>
			<h4>Step: ${stepCount}</h4><a href="http://localhost:8080/getCount/20000"><button>Get Count so far</button></a>
			<h4>Step: ${stepMax}</h4><a href="http://localhost:8080/getMax/35200"><button>Get Maximum</button></a>
			<h4>Step: ${stepMin}</h4><a href="http://localhost:8080/getMin/70000"><button>Get Minimum</button></a>
		</div>

	</div>
	<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>