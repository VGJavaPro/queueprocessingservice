package com.javapro.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {

	@Value("${step.createAndLoadData}")
	private String stepLoadData = "";
	
	@Value("${step.runService}")
	private String stepRunService = "";
	
	@Value("${step.count}")
	private String stepCount = "";
	
	@Value("${step.max}")
	private String stepMax = "";
	
	@Value("${step.min}")
	private String stepMin = "";

	@RequestMapping("/")
	public String welcome(Map<String, Object> model) {
		model.put("stepLoadData", this.stepLoadData);
		model.put("stepRunService", this.stepRunService);
		model.put("stepCount", this.stepCount);
		model.put("stepMax", this.stepMax);
		model.put("stepMin", this.stepMin);
		return "welcome";
	}

}