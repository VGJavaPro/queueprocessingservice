package com.javapro.controller;

import java.io.File;
import java.util.Optional;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javapro.service.Processor;
import com.javapro.util.LoadDataToFile;
import com.javapro.util.ServiceUtil;

@RestController
public class QueueProcessingController {	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ServiceUtil serviceUtil;
		
	@RequestMapping("/createAndLoadData/{size}")
	public String createAndLoadData(@PathVariable int size){
		File directory = new File(serviceUtil.getDirPath());
	    if (! directory.exists()){
	        directory.mkdir();
	    }
		LoadDataToFile.loadFile(size,serviceUtil.getFilePath());
		log.debug("Data has been created successfully");
		return "Data has been created successfully";
	}

	@RequestMapping("/process")
	public String process(){
		//Data file exists. Call the service.
		boolean isFileExists = false;
		if (new File(serviceUtil.getFilePath()).exists())
		{
			isFileExists = true;
		}
		if(isFileExists){
			Runnable runnable = () -> {
				try {
					Processor processor = new Processor(serviceUtil);
					log.debug("Starting the service");
					processor.startService();
					log.debug("service has been started");
				}
				catch (Exception e) {
					log.info("Exception during service startup");
				}
			};

			new Thread(runnable).start();
		}
		return "Processing.....";
	}

	/***
	 * This method returns all the records processed so far
	 * @return  returns the record count
	 */
	@GetMapping("/count")
	public long count(){
		Queue<Integer> totalRecordsProcessed = new Processor(serviceUtil).getOutputList();
		if(null == totalRecordsProcessed || totalRecordsProcessed.isEmpty()){
			return 0;
		}
		long count = totalRecordsProcessed.parallelStream().count();
		log.info("Count so far :: " + count);
		return count;
	}

	/***
	 * This method returns all the records processed so far after a cutoff
	 * @return  returns the record count
	 */
	@GetMapping( "/getCount/{cutoff}")
	public String countByCutOff(@PathVariable int cutoff){
		Queue<Integer> recordsProcessed = new Processor(serviceUtil).getOutputList();

		if(null == recordsProcessed || recordsProcessed.isEmpty()){
			return "No Records Found";
		}
		long countSoFar = recordsProcessed.parallelStream().filter(x -> x > cutoff).count();
		log.debug("Count So Far  :: " + countSoFar);
		String returnResult = "The Count of numbers processed so far after cutoff " + cutoff + " is " + countSoFar;
		return returnResult;
	}


	/***
	 * This method returns maximum value of in records processed so far after a cutoff
	 * @return  returns the record count
	 */
	@GetMapping("/getMax/{cutoff}")
	public String getMax(@PathVariable int cutoff){
		Queue<Integer> recordsProcessed = new Processor(serviceUtil).getOutputList();
		if(null == recordsProcessed || recordsProcessed.isEmpty()){
			return "No Records Found";
		}
		Optional<Integer> maxIntValue = recordsProcessed.parallelStream().filter(x -> x > cutoff).reduce(Integer::max);
		String returnResult =  null;
		if(maxIntValue.isPresent()){
			returnResult = "The Maximum number after cutoff " + cutoff + " is " + maxIntValue.get();
		} else {
			returnResult = "No Maximum value found";
		}
		log.debug("Max Int  :: " + maxIntValue.get());
		return returnResult;
	}


	/***
	 * This method returns minimum value of in records processed so far after a cutoff
	 * @return  returns the record count
	 */
	@GetMapping("/getMin/{cutoff}")
	public String getMin(@PathVariable int cutoff){
		Queue<Integer> recordsProcessed = new Processor(serviceUtil).getOutputList();
		if(null == recordsProcessed || recordsProcessed.isEmpty()){
			return "No Records Found";
		}
		Optional<Integer>  minIntValue = recordsProcessed.parallelStream().filter(x -> x > cutoff).reduce(Integer::min);
		String returnResult = null;
		if(minIntValue.isPresent()){
			returnResult = "The Minimum number after cutoff " + cutoff + " is " + minIntValue.get();
		} else {
			returnResult = "No Minimum value found";
		}
		log.debug("Minimum number  :: " +  minIntValue.get());
		
		return returnResult;
	}

}
