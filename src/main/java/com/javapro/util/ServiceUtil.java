package com.javapro.util;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.javapro.util")
public class ServiceUtil {
	@Value( "${windows.datafile.dir}" )
	private String windowsDatafileDir;
	
	@Value( "${windows.datafile.path}" )
	private String windowsDatafilePath;

	@Value("${linux.datafile.path}")
	private String linuxDatafilePath;

	@Value("${queue.size}")
	private String queueSize;
	
	@Value("${number.of.consumers}")
	private String numberOfConsumers;

	/**
	 * Detect the operating system from the os.name System property 
	 * 
	 * @returns - the file path
	 */
	public   String getFilePath() {
		String filePath = null;
		String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
		if (OS.indexOf("win") >= 0) {
			filePath = windowsDatafilePath;
		} else if (OS.indexOf("nux") >= 0) {
			filePath = linuxDatafilePath;
		} else {
			filePath = null;
		}

		return filePath; 
	}  
	
	public String getDirPath(){
		return  windowsDatafileDir;
	}
	
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}

	public String getQueueSize(){
		return queueSize;
	}
	
	public String getNumberOfConsumers(){
		return numberOfConsumers;
	}

}