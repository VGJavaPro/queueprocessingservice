package com.javapro.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class LoadDataToFile {

	public static void loadFile(int recSize, String datafilePath) {
		// The target file
		int i = 1;
		File out = new File(datafilePath);
		FileWriter fw = null;
		int n = recSize;
		try {
			// Create file writer object
			fw = new FileWriter(out);
			// Wrap the writer with buffered streams
			BufferedWriter writer = new BufferedWriter(fw);
			int line;
			//Random random = new Random();
			String nextLineStr = null;
			while (n > 0) {
				// Randomize an integer and write it to the output file
				// line = random.nextInt(1000);
				line = recSize - n;     //Integer from 1 .. recSize 
				nextLineStr =  (recSize > line + 1) ? "\n" : "";
				writer.write(line+1 + nextLineStr);
				n--;
			}
			// Close the stream
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}


}