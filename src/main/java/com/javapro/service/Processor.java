package com.javapro.service;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingDeque;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.javapro.util.ServiceUtil;

@Component
public class Processor {
	private static BlockingQueue<String> queue;
	private static Queue<Integer> processedRecords = new ConcurrentLinkedQueue<Integer>();
	private static Collection<Thread> producerThreadCollection, allThreadCollection;
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private ServiceUtil serviceUtil;
	
	public Processor(){
		
	}
	
	public Processor(ServiceUtil serviceUtil){
		this.serviceUtil = serviceUtil;
	}
		
	public  void  startService(){
		log.info("Service running !!!");
		try {
			producerThreadCollection = new ArrayList<Thread>();
			allThreadCollection = new ArrayList<Thread>();
			queue = new LinkedBlockingDeque<String>(Integer.parseInt(serviceUtil.getQueueSize()));
		
			createAndStartProducers();

			createAndStartConsumers();

			for(Thread t: allThreadCollection){
				try {
					t.join();
				} catch (InterruptedException e) {
					 log.error("Exception during join" + e.getMessage());;
				}
			}
			
			log.info("Service is finished");
		} catch (Exception e) {
			 log.error(e.getMessage());
		} finally {
			log.info("Service is Done !!!");
		} 
	}


	private  void createAndStartProducers(){
		Producer producer = new Producer(Paths.get(serviceUtil.getFilePath()), queue);
		Thread producerThread = new Thread(producer,"producer");
		producerThreadCollection.add(producerThread);
		producerThread.start();
		allThreadCollection.addAll(producerThreadCollection);
	}

	private  void createAndStartConsumers(){
		for(int i = 0; i < Integer.parseInt(serviceUtil.getNumberOfConsumers()); i++){
			Thread consumerThread = new Thread(new Consumer(queue, processedRecords, serviceUtil), "consumer-"+i+1);
			allThreadCollection.add(consumerThread);
			consumerThread.start();
		}
	}

	public  boolean isProducerAlive(){
		for(Thread t: producerThreadCollection){
			if(t.isAlive())
				return true;
		}
		return false;
	}

	public Queue<Integer> getOutputList(){
		return processedRecords;
	}
	
}
