package com.javapro.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Producer implements Runnable{

    private Path fileToRead;
    private BlockingQueue<String> queue;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public Producer(Path filePath, BlockingQueue<String> q){
        fileToRead = filePath;
        queue = q;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = Files.newBufferedReader(fileToRead);
            String line;
            while((line = reader.readLine()) != null){
                try {
                    queue.put(line);
                 	log.debug("Put record on Queue::" + line);
                } catch (InterruptedException e) {
                	log.error("Thread has been Interrupted" + e.getMessage());
                }
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}