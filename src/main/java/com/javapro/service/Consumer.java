package com.javapro.service;

import java.util.Queue;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javapro.util.ServiceUtil;

public class Consumer implements Runnable{
	private BlockingQueue<String> queue;
	private final Logger log = LoggerFactory.getLogger(this.getClass());
    private ServiceUtil serviceUtil = null;
    private Queue<Integer> processedRecords;
	public Consumer(BlockingQueue<String> q, Queue<Integer> oList, ServiceUtil serviceUtil){
		queue = q;
		processedRecords = oList;
		this.serviceUtil = serviceUtil;
	}

	public void run(){
		String line;
		int digit = 0;
		while(true){
			try {
				line = queue.take();
				if(ServiceUtil.isNumeric(line)){
					digit = Integer.parseInt(line);
				} else {
					continue;
				}
				log.debug("Took record from Queue::" + line);
				if(line == null && !new Processor(serviceUtil).isProducerAlive())
					return;
				if(line != null){
					processedRecords.add(digit);
					log.info("Processed Record ::" + line);
				}

			} catch (InterruptedException e) {
				log.error("Thread has been Interupted" + e.getMessage());
			}

		}

	}
}