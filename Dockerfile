FROM java:8
VOLUME /tmp
EXPOSE 8181
ADD /target/QueueProcessingService-0.0.1-SNAPSHOT.jar QProcess.war
RUN bash -c 'touch / QProcess.war'
ENTRYPOINT ["java","-jar"," QProcess.war"]